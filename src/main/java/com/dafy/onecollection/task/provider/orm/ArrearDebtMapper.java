package com.dafy.onecollection.task.provider.orm;

import com.dafy.onecollection.task.provider.pojo.ArrearDebt;
import com.dafy.onecollection.task.provider.pojo.ArrearDebtExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ArrearDebtMapper {
    int countByExample(ArrearDebtExample example);

    int deleteByExample(ArrearDebtExample example);

    int deleteByPrimaryKey(Long id);

    int insert(ArrearDebt record);

    int insertSelective(ArrearDebt record);

    List<ArrearDebt> selectByExample(ArrearDebtExample example);

    ArrearDebt selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") ArrearDebt record, @Param("example") ArrearDebtExample example);

    int updateByExample(@Param("record") ArrearDebt record, @Param("example") ArrearDebtExample example);

    int updateByPrimaryKeySelective(ArrearDebt record);

    int updateByPrimaryKey(ArrearDebt record);
}